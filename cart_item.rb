class CartItem
  attr_accessor :item_code, :item_name, :item_price, :discounted_price, :discount_until
  
  def initialize(code, name, price)
    @item_code = code
    @item_name = name
    @item_price = price
    @discounted_price = price
    @discount_until = Time.now
  end
  
  
  def price
    if Time.now < @discount_until
      @discounted_price
    else
      @item_price
    end
  end 
end
