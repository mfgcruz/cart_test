require_relative "shopping_cart"
require_relative "cart_item"
require "test/unit"
 
class TestSimpleCart < Test::Unit::TestCase

  # item_sml = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
  # item_med = CartItem.new('ult_medium', 'Unlimited 2GB', 29.9)
  # item_lrg = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
  # item_1gb = CartItem.new('1gb', '1 GB Data-pack', 9.9)
  
  # pricing_rules = []
  # pricing_rules << {:coupon_code => "I<3AMAYSIM", :discount => 10.0, :duration => nil, :ctype => "ALL"}
  # pricing_rules << {:coupon_code => "3for2_ult_small", :discount => 100.0, :item_code => "ult_small", :duration => 2, :ctype => "ITEM_DISCOUNT"}
  # pricing_rules << {:coupon_code => "unli5gb_bulk", :bulk_price => 39.9, :item_code => "ult_large", :duration => 1, :ctype => "BULK"}
  # pricing_rules << {:coupon_code => "unli2gb_freebie", :item_code => "1gb", :ctype => "FREE"}
  
  def test_scenario_1
    puts "TESTING SCENARIO 1 \n"
    item1 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item2 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item3 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item4 = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
    
    pricing_rules = []
    pricing_rules << {:coupon_code => "I<3AMAYSIM", :discount => 10.0, :duration => nil, :ctype => "ALL"}
    pricing_rules << {:coupon_code => "3for2_ult_small", :discount => 100.0, :item_code => "ult_small", :duration => 2, :ctype => "ITEM_DISCOUNT"}
    pricing_rules << {:coupon_code => "unli5gb_bulk", :bulk_price => 39.9, :item_code => "ult_large", :duration => 1, :ctype => "BULK"}
    pricing_rules << {:coupon_code => "unli2gb_freebie", :item_code => "1gb", :ctype => "FREE"}
    
    cart1 = ShoppingCart.new(pricing_rules)
    
    cart1.add(item1)
    cart1.add(item2)
    cart1.add(item3)
    cart1.add(item4)
    
    assert_equal(94.70, cart1.total)
    cart1.items
    puts "TOTAL: $#{cart1.total}"
  end
  
  def test_scenario_2
    puts "TESTING SCENARIO 2 \n"
    
    item1 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item2 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item3 = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
    item4 = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
    item5 = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
    item6 = CartItem.new('ult_large', 'Unlimited 5GB', 44.9)
    
    pricing_rules = []
    pricing_rules << {:coupon_code => "I<3AMAYSIM", :discount => 10.0, :duration => nil, :ctype => "ALL"}
    pricing_rules << {:coupon_code => "3for2_ult_small", :discount => 100.0, :item_code => "ult_small", :duration => 2, :ctype => "ITEM_DISCOUNT"}
    pricing_rules << {:coupon_code => "unli5gb_bulk", :bulk_price => 39.9, :item_code => "ult_large", :duration => 1, :ctype => "BULK"}
    pricing_rules << {:coupon_code => "unli2gb_freebie", :item_code => "1gb", :ctype => "FREE"}
    
    cart1 = ShoppingCart.new(pricing_rules)
    
    cart1.add(item1)
    cart1.add(item2)
    cart1.add(item3)
    cart1.add(item4)
    cart1.add(item5)
    cart1.add(item6)
  
    assert_equal(209.40, cart1.total)
    cart1.items
    puts "TOTAL: $#{cart1.total}"
  end
  
  def test_scenario_3
    puts "TESTING SCENARIO 3 \n"
    
    item1 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    item2 = CartItem.new('ult_medium', 'Unlimited 2GB', 29.9)
    item3 = CartItem.new('ult_medium', 'Unlimited 2GB', 29.9)
    
    pricing_rules = []
    pricing_rules << {:coupon_code => "I<3AMAYSIM", :discount => 10.0, :duration => nil, :ctype => "ALL"}
    pricing_rules << {:coupon_code => "3for2_ult_small", :discount => 100.0, :item_code => "ult_small", :duration => 2, :ctype => "ITEM_DISCOUNT"}
    pricing_rules << {:coupon_code => "unli5gb_bulk", :bulk_price => 39.9, :item_code => "ult_large", :duration => 1, :ctype => "BULK"}
    pricing_rules << {:coupon_code => "unli2gb_freebie", :item_code => "1gb", :ctype => "FREE"}
    
    cart1 = ShoppingCart.new(pricing_rules)
    
    cart1.add(item1)
    cart1.add(item2)
    cart1.add(item3)
    
    assert_equal(84.70, cart1.total)
    cart1.items
    puts "TOTAL: $#{cart1.total}"
  end
  
  def test_scenario_4
    puts "TESTING SCENARIO 4 \n"
    
    item1 = CartItem.new('1gb', '1 GB Data-pack', 9.9)
    item2 = CartItem.new('ult_small', 'Unlimited 1GB', 24.9)
    
    pricing_rules = []
    pricing_rules << {:coupon_code => "I<3AMAYSIM", :discount => 10.0, :duration => nil, :ctype => "ALL"}
    pricing_rules << {:coupon_code => "3for2_ult_small", :discount => 100.0, :item_code => "ult_small", :duration => 2, :ctype => "ITEM_DISCOUNT"}
    pricing_rules << {:coupon_code => "unli5gb_bulk", :bulk_price => 39.9, :item_code => "ult_large", :duration => 1, :ctype => "BULK"}
    pricing_rules << {:coupon_code => "unli2gb_freebie", :item_code => "1gb", :ctype => "FREE"}
    
    cart1 = ShoppingCart.new(pricing_rules)
    
    cart1.add(item1)
    cart1.add(item2, "I<3AMAYSIM")
    
    assert_equal(31.32, cart1.total)
    cart1.items
    puts "TOTAL: $#{cart1.total}"
  end
 
end
