class ShoppingCart
  attr_accessor :cart_items, :pricing_rules

  def initialize(pricing_rule)
    @cart_items = []
    @pricing_rules = pricing_rule
    @active_code = ""
  end
  
  def add(item, promo_code = "")
    @cart_items << item
    @active_code = promo_code
  end
  
  def update_item_pricing(item, code)
    rule = @pricing_rules.select{|rule| rule[:coupon_code] == code}[0]
    now = Time.now
    
    case rule[:ctype]
      when "ALL"
      when "ITEM_DISCOUNT"
        item.discounted_price = item.item_price - item.item_price*(rule[:discount]/100.0)
        item.discount_until = Time.new(now.year, now.month + rule[:duration], now.day)
      when "BULK"
        item.discounted_price = rule[:bulk_price]
        item.discount_until = Time.new(now.year, now.month + rule[:duration], now.day)
      when "FREE"
    end
  end
  
  def total
    item_codes = @cart_items.collect{|item| item.item_code}
    
    if item_codes.include? "ult_small"
      ult_small_items = @cart_items.select{|item| item.item_code == "ult_small"}
      
      for i in 0...(ult_small_items.size / 3)
        item = ult_small_items[i*3]
        update_item_pricing(item, "3for2_ult_small")
      end
    end
    
    if item_codes.include? "ult_medium"
      ult_medium_items = @cart_items.select{|item| item.item_code == "ult_medium"}
      
      for i in 0...(ult_medium_items.size)
        @cart_items << CartItem.new('1gb', '1 GB Data-pack', 0.0)
      end
    end
    
    if item_codes.include? "ult_large"
      ult_large_items = @cart_items.select{|item| item.item_code == "ult_large"}
      if ult_large_items.size > 3
        ult_large_items.each do |item|
          update_item_pricing(item, "unli5gb_bulk")
        end
      end
    end
    
    item_totals = @cart_items.inject(0){|sum, item| sum + item.price}
    unless @active_code == ""
      rule = @pricing_rules.select{|rule| rule[:coupon_code] == @active_code}[0]
      item_totals = item_totals - item_totals*(rule[:discount]/100.0)
    end
    
    item_totals.round(2)
  end
  
  def items
    grouped_items = @cart_items.group_by{|item| item.item_code}
    grouped_items.each do |key, group|
      puts "#{group.size} x #{group[0].item_name}"
    end
    
  end
end
